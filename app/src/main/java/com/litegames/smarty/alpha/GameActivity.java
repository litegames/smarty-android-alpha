package com.litegames.smarty.alpha;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.litegames.smarty.alpha.game.tictactoe.TTTAIGamePlayer;
import com.litegames.smarty.alpha.game.tictactoe.TTTGame;
import com.litegames.smarty.alpha.game.tictactoe.TTTGamePlayerInfo;
import com.litegames.smarty.alpha.game.tictactoe.TTTGameState;
import com.litegames.smarty.alpha.game.tictactoe.TTTGameStateSerializer;
import com.litegames.smarty.alpha.game.tictactoe.TTTGameUI;
import com.litegames.smarty.alpha.game.tictactoe.TTTHumanGamePlayer;
import com.litegames.smarty.game.Game;
import com.litegames.smarty.game.GameHost;
import com.litegames.smarty.game.GameLocalHost;
import com.litegames.smarty.game.GamePlayer;
import com.litegames.smarty.game.SmartyGameRemoteHost;
import com.litegames.smarty.game.SmartyLocalPlayer;
import com.litegames.smarty.game.SmartyRemotePlayer;
import com.litegames.smarty.sdk.GameRoom;
import com.litegames.smarty.sdk.Resource;
import com.litegames.smarty.sdk.ResourceSize;
import com.litegames.smarty.sdk.ResourcesProvider;
import com.litegames.smarty.sdk.Smarty;
import com.litegames.smarty.sdk.SmartyProvider;
import com.litegames.smarty.sdk.User;
import com.litegames.smarty.sdk.UserVariable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class GameActivity extends AppCompatActivity implements OnClickListener, TTTGameUI {

    public enum GameMode {
        SINGLE_PLAYER,
        HOT_SEAT,
        MULTI_PLAYER
    }

    public static final String EXTRA_GAME_MODE = "gameMode";
    public static final String EXTRA_GAME_ROOM_ID = "gameRoomId";

    private static final Logger logger = LoggerFactory.getLogger(GameActivity.class);

    private static final String TURN_MARKER_LEFT = "◁";
    private static final String TURN_MARKER_RIGHT = "▷";

    private class PlayerViews {
        public TextView label;
        public TextView marker;
        public ImageView avatar;
    }

    private GameMode gameMode;
    private GameHost gameHost;
    private boolean performingMove;

    private ViewGroup topPanel;
    private List<PlayerViews> playersViews;
    private TextView turnMarker;
    private List<List<Button>> fieldsButtons;
    private TextView messageLabel;
    private Button quitButton;

    private TTTGameUI.EventListener eventListener;

    public static Intent intentForDefaultActionSinglePlayerMode(Context context) {
        Intent intent = new Intent(context, GameActivity.class);
        intent.putExtra(GameActivity.EXTRA_GAME_MODE, GameMode.SINGLE_PLAYER);
        return intent;
    }

    public static Intent intentForDefaultActionHotSeatMode(Context context) {
        Intent intent = new Intent(context, GameActivity.class);
        intent.putExtra(GameActivity.EXTRA_GAME_MODE, GameMode.HOT_SEAT);
        return intent;
    }

    public static Intent intentForDefaultActionMultiplayerMode(Context context, GameRoom gameRoom) {
        Intent intent = new Intent(context, GameActivity.class);
        intent.putExtra(GameActivity.EXTRA_GAME_MODE, GameMode.MULTI_PLAYER);
        intent.putExtra(GameActivity.EXTRA_GAME_ROOM_ID, gameRoom.getId());
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game);

        // Init views
        topPanel = (ViewGroup) findViewById(R.id.top_panel);

        playersViews = new ArrayList<>();
        {
            PlayerViews pv = new PlayerViews();
            pv.label = (TextView) findViewById(R.id.player1_label);
            pv.marker = (TextView) findViewById(R.id.player1_marker);
            pv.avatar = (ImageView) findViewById(R.id.player1_avatar);
            playersViews.add(pv);

            pv.label.setText("");
            pv.marker.setText("");
            pv.avatar.setImageDrawable(null);
        }
        {
            PlayerViews pv = new PlayerViews();
            pv.label = (TextView) findViewById(R.id.player2_label);
            pv.marker = (TextView) findViewById(R.id.player2_marker);
            pv.avatar = (ImageView) findViewById(R.id.player2_avatar);
            playersViews.add(pv);

            pv.label.setText("");
            pv.marker.setText("");
            pv.avatar.setImageDrawable(null);
        }

        turnMarker = (TextView) findViewById(R.id.turn_marker);
        {
            List<List<Integer>> fieldsButtonsIds = Arrays.asList(
                    Arrays.asList(R.id.game_cell_1, R.id.game_cell_2, R.id.game_cell_3),
                    Arrays.asList(R.id.game_cell_4, R.id.game_cell_5, R.id.game_cell_6),
                    Arrays.asList(R.id.game_cell_7, R.id.game_cell_8, R.id.game_cell_9)
            );

            fieldsButtons = new ArrayList<>();
            for (List<Integer> fieldsButtonsIdsRow : fieldsButtonsIds) {
                List<Button> buttonsRow = new ArrayList<>();
                for (Integer id : fieldsButtonsIdsRow) {
                    Button button = (Button) findViewById(id);
                    button.setOnClickListener(this);
                    button.setEnabled(false);

                    buttonsRow.add(button);
                }
                fieldsButtons.add(buttonsRow);
            }
        }
        messageLabel = (TextView) findViewById(R.id.message_label);
        quitButton = (Button) findViewById(R.id.button_exit);
        quitButton.setOnClickListener(this);

        topPanel.setVisibility(View.INVISIBLE);
        for (int x = 0; x < TTTGame.BOARD_WIDTH; x++) {
            for (int y = 0; y < TTTGame.BOARD_HEIGHT; y++) {
                Button button = fieldsButtons.get(x).get(y);
                button.setText("");
            }
        }
        messageLabel.setText(R.string.game_message_waiting_ready);
        quitButton.setEnabled(false);

        initGame();
//        // Simulate loading lag
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                initGame();
//            }
//        }, 3000);
    }

    private void initGame() {
        Smarty smarty = SmartyProvider.getInstance().getSmarty();


        gameMode = (GameMode) getIntent().getSerializableExtra(EXTRA_GAME_MODE);
        int gameRoomId = getIntent().getIntExtra(EXTRA_GAME_ROOM_ID, -1);


        switch (gameMode) {
            case SINGLE_PLAYER:
                gameHost = createSinglePlayerGame(smarty);
                break;

            case HOT_SEAT:
                gameHost = createHotSeatGame();
                break;

            case MULTI_PLAYER:
                gameHost = createMultiPlayerGame(smarty, gameRoomId);
                break;
        }

        gameHost.start();
    }

    private GameLocalHost createSinglePlayerGame(Smarty smarty) {
        ResourcesProvider resourcesProvider = new ResourcesProvider(this);

        List<GamePlayer> players = new ArrayList<>();
        List<TTTGamePlayerInfo> playersInfos = new ArrayList<>();

        {
            int playerId = 0;

            GamePlayer player = new TTTHumanGamePlayer(playerId, playersInfos, this, true, true);
            players.add(player);

            TTTGamePlayerInfo pi = new TTTGamePlayerInfo();
            pi.id = playerId;
            pi.name = getString(R.string.game_text_me);
            if (smarty.isAuthenticated()) {
                pi.avatar = resourcesProvider.getAvatarResource(smarty.getMySelf().getProfile(), ResourceSize.SMALL);
            } else {
                pi.avatar = resourcesProvider.getAvatarForOfflineUsageResource(ResourceSize.SMALL);
            }

            if (smarty.isAuthenticated()) {
                pi.color = resourcesProvider.getAvatarDominantColor(smarty.getMySelf().getProfile());
            } else {
                pi.color = resourcesProvider.getAvatarForOfflineUsageDominantColor();
            }
            pi.mySelf = true;
            playersInfos.add(pi);
        }

        {
            int playerId = 1;

            GamePlayer player = new TTTAIGamePlayer(playerId);
            players.add(player);

            TTTGamePlayerInfo pi = new TTTGamePlayerInfo();
            pi.id = playerId;
            pi.name = getString(R.string.game_text_ai);
            pi.avatar = resourcesProvider.getAvatarForAIPlayerResource(ResourceSize.SMALL);
            pi.color = resourcesProvider.getAvatarForAIPlayerDominantColor();
            playersInfos.add(pi);
        }

        Game game = new TTTGame(players);
        return new GameLocalHost(game, players);
    }

    private GameLocalHost createHotSeatGame() {
        ResourcesProvider resourcesProvider = new ResourcesProvider(this);

        List<GamePlayer> players = new ArrayList<>();
        List<TTTGamePlayerInfo> playersInfos = new ArrayList<>();
        List<Integer> playersColors = resourcesProvider.getNUniqueAvatarDominantColors(2);

        {
            int playerId = 0;

            GamePlayer player = new TTTHumanGamePlayer(playerId, playersInfos, this, true, false);
            players.add(player);

            TTTGamePlayerInfo pi = new TTTGamePlayerInfo();
            pi.id = playerId;
            pi.name = getString(R.string.game_text_player_1);
            pi.avatar = resourcesProvider.getAvatarForOfflineUsageResource(ResourceSize.SMALL);
            pi.color = playersColors.get(0);
            playersInfos.add(pi);
        }

        {
            int playerId = 1;

            GamePlayer player = new TTTHumanGamePlayer(playerId, playersInfos, this, false, false);
            players.add(player);

            TTTGamePlayerInfo pi = new TTTGamePlayerInfo();
            pi.id = playerId;
            pi.name = getString(R.string.game_text_player_2);
            pi.avatar = resourcesProvider.getAvatarForOfflineUsageResource(ResourceSize.SMALL);
            pi.color = playersColors.get(1);
            playersInfos.add(pi);
        }

        Game game = new TTTGame(players);
        return new GameLocalHost(game, players);
    }

    private GameHost createMultiPlayerGame(Smarty smarty, int gameRoomId) {
        ResourcesProvider resourcesProvider = new ResourcesProvider(this);

        List<GamePlayer> players = new ArrayList<>();
        List<TTTGamePlayerInfo> playersInfos = new ArrayList<>();

        GameRoom gameRoom = smarty.getGameManager().getGameRoom();

        boolean meStarts = gameRoom.isMeStarts();

        logger.debug("Game start. meStarts: {}", meStarts);

        Collection<User> joinedUsers = gameRoom.getJoinedUsers();
        User mySelf = smarty.getMySelf();
        User opponent = null;
        for (User u : joinedUsers) {
            if (!u.equals(mySelf)) {
                opponent = u;
            }
        }

        // Example of handling new, backward-compatible, fictional 'tie proposals' feature.
        {
            boolean supportsTieProposals = true;
            for (User u : joinedUsers) {
                UserVariable uv = u.getUserVariable("supportsTieProposals");
                supportsTieProposals &= uv != null && uv.getBoolValue() != null && uv.getBoolValue();
            }
            logger.info("supportsTieProposals: {}", supportsTieProposals);
        }

        GameHost gameHost;
        if (meStarts) {
            int pl1Color = resourcesProvider.getAvatarDominantColor(mySelf.getProfile());
            int pl2Color = resourcesProvider.getAvatarDominantColor(opponent.getProfile());
            String myAvatar = mySelf.getProfile().getAvatar();
            String opponentAvatar = opponent.getProfile().getAvatar();
            if ((myAvatar == null && opponentAvatar == null) || (myAvatar != null && opponentAvatar != null && myAvatar.equals(opponentAvatar))) {
                List<Integer> colors = resourcesProvider.getNUniqueAvatarDominantColors(2);
                pl1Color = colors.get(0);
                pl2Color = colors.get(1);
            }

            {
                int playerId = mySelf.getId();

                GamePlayer player = new TTTHumanGamePlayer(playerId, playersInfos, this, true, true);
                players.add(new SmartyLocalPlayer(smarty, gameRoom, player));

                TTTGamePlayerInfo pi = new TTTGamePlayerInfo();
                pi.id = playerId;
                pi.name = mySelf.getProfile().getDisplayName();
                pi.avatar = resourcesProvider.getAvatarResource(mySelf.getProfile(), ResourceSize.SMALL);
                pi.color = pl1Color;
                pi.mySelf = true;
                playersInfos.add(pi);
            }

            {
                int playerId = opponent.getId();

                GamePlayer player = new SmartyRemotePlayer(smarty, gameRoom, opponent.getId(), new TTTGameStateSerializer());
                players.add(player);

                TTTGamePlayerInfo pi = new TTTGamePlayerInfo();
                pi.id = playerId;
                pi.name = opponent.getProfile().getDisplayName();
                pi.avatar = resourcesProvider.getAvatarResource(opponent.getProfile(), ResourceSize.SMALL);
                pi.color = pl2Color;
                pi.mySelf = false;
                playersInfos.add(pi);
            }

            Game game = new TTTGame(players);
            gameHost = new GameLocalHost(game, players);
        } else {
            List<Integer> playerIds = new ArrayList<>();

            int pl1Color = resourcesProvider.getAvatarDominantColor(mySelf.getProfile());
            int pl2Color = resourcesProvider.getAvatarDominantColor(opponent.getProfile());
            String myAvatar = mySelf.getProfile().getAvatar();
            String opponentAvatar = opponent.getProfile().getAvatar();
            if ((myAvatar == null && opponentAvatar == null) || (myAvatar != null && opponentAvatar != null && myAvatar.equals(opponentAvatar))) {
                List<Integer> colors = resourcesProvider.getNUniqueAvatarDominantColors(2);
                pl1Color = colors.get(0);
                pl2Color = colors.get(1);
            }

            GamePlayer humanPlayer;
            {
                int playerId = mySelf.getId();
                playerIds.add(playerId);

                humanPlayer = new TTTHumanGamePlayer(playerId, playersInfos, this, true, true);
                players.add(humanPlayer);

                TTTGamePlayerInfo pi = new TTTGamePlayerInfo();
                pi.id = playerId;
                pi.name = mySelf.getProfile().getDisplayName();
                pi.avatar = resourcesProvider.getAvatarResource(mySelf.getProfile(), ResourceSize.SMALL);
                pi.color = pl1Color;
                pi.mySelf = true;
                playersInfos.add(pi);
            }

            {
                int playerId = opponent.getId();

                playerIds.add(playerId);

                TTTGamePlayerInfo pi = new TTTGamePlayerInfo();
                pi.id = playerId;
                pi.name = opponent.getProfile().getDisplayName();
                pi.avatar = resourcesProvider.getAvatarResource(opponent.getProfile(), ResourceSize.SMALL);
                pi.color = pl2Color;
                pi.mySelf = false;
                playersInfos.add(pi);
            }

            TTTGameState gameState = new TTTGameState(playerIds);

            gameHost = new SmartyGameRemoteHost(smarty, gameRoom, humanPlayer, gameState, new TTTGameStateSerializer());
        }
        return gameHost;
    }

    @Override
    public void onBackPressed() {
        onClick(quitButton);
    }

    @Override
    public void onClick(View v) {
        if (quitButton.equals(v)) {
            setBoardEnabled(false);
            notifyQuit();
        } else {
            for (int x = 0; x < TTTGame.BOARD_WIDTH; x++) {
                for (int y = 0; y < TTTGame.BOARD_HEIGHT; y++) {
                    if (v.equals(fieldsButtons.get(x).get(y))) {
                        onMove(x, y);
                    }
                }
            }
        }
    }

    @Override
    public void performMove() {
        if (performingMove) {
            throw new AssertionError("Should not be performing move");
        }

        performingMove = true;
        setBoardEnabled(true);
    }

    private void onMove(int x, int y) {
        if (!performingMove) return;

        // Get only first button click if multiple where clicked
        setBoardEnabled(false);
        performingMove = false;

        notifyMove(x, y);
    }

    @Override
    public void showGameState(List<TTTGamePlayerInfo> playersInfos, TTTGameState gameState) {
        topPanel.setVisibility(View.VISIBLE);

        // Sort players: myself on left, otherwise lower id on left
        List<TTTGamePlayerInfo> gpis = new ArrayList<>(playersInfos);
        Collections.sort(gpis, new Comparator<TTTGamePlayerInfo>() {
            @Override
            public int compare(TTTGamePlayerInfo lhs, TTTGamePlayerInfo rhs) {
                if (lhs.mySelf) {
                    return -1;
                } else if (rhs.mySelf) {
                    return 1;
                } else {
                    return lhs.id < rhs.id ? -1 : (lhs.id == rhs.id ? 0 : 1);
                }
            }
        });

        Map<TTTGame.Mark, Integer> markColorMap = new HashMap<>();
        for (int i = 0; i < gpis.size(); i++) {
            TTTGamePlayerInfo gamePlayerInfo = playersInfos.get(i);
            TTTGame.Mark mark = gameState.getPlayerState(gamePlayerInfo.id).mark;

            PlayerViews pv = playersViews.get(i);
            pv.label.setText(gamePlayerInfo.name);

            Resource avatarResource = (Resource) gamePlayerInfo.avatar;
            avatarResource.load(this).into(pv.avatar);

            pv.marker.setTextColor(gamePlayerInfo.color);

            if (mark != null) {
                pv.marker.setText(getString(mark.equals(TTTGame.Mark.CROSS) ? R.string.game_mark_cross : R.string.game_mark_nought));
                markColorMap.put(mark, gamePlayerInfo.color);
            }
        }

        if (gameState.playState.equals(TTTGameState.PlayState.PLAYING)) {
            int currentPlayerIdx = 0;
            for (int i = 0; i < gpis.size(); i++) {
                TTTGamePlayerInfo gamePlayerInfo = playersInfos.get(i);
                TTTGame.Mark mark = gameState.getPlayerState(gamePlayerInfo.id).mark;

                if (gameState.currentMark.equals(mark)) {
                    currentPlayerIdx = i;
                    break;
                }
            }

            if (currentPlayerIdx == 0) {
                turnMarker.setText(TURN_MARKER_LEFT);
            } else {
                turnMarker.setText(TURN_MARKER_RIGHT);
            }
        } else {
            turnMarker.setText("");
        }

        for (int x = 0; x < TTTGame.BOARD_WIDTH; x++) {
            for (int y = 0; y < TTTGame.BOARD_HEIGHT; y++) {
                TTTGame.Mark mark = gameState.board[x][y];

                Button button = fieldsButtons.get(x).get(y);

                if (mark != null) {
                    if (mark.equals(TTTGame.Mark.CROSS)) {
                        button.setText(getString(R.string.game_mark_cross));
                    } else {
                        button.setText(getString(R.string.game_mark_nought));
                    }
                    button.setTextColor(markColorMap.get(mark));
                } else {
                    button.setText("");
                }
            }
        }

        if (gameState.playState.equals(TTTGameState.PlayState.PLAYING)) {
            int playerId = gameState.idOfPlayerWithMark(gameState.currentMark);
            TTTGamePlayerInfo playerInfo = getGamePlayerInfo(playerId, playersInfos);

            if (!gameMode.equals(GameMode.HOT_SEAT)) {
                if (playerInfo.mySelf) {
                    messageLabel.setText(getString(R.string.game_message_my_turn));
                } else {
                    messageLabel.setText(getString(R.string.game_message_opponent_turn));
                }
            } else {
                messageLabel.setText("");
            }
        } else {
            messageLabel.setText("");
        }

        if (gameState.playState.equals(TTTGameState.PlayState.PLAYING)) {
            quitButton.setEnabled(true);
        } else {
            quitButton.setEnabled(false);
        }
    }

    @Override
    public void finish(List<TTTGamePlayerInfo> gamePlayersInfos, TTTGameState gameState) {
        String msg;
        String msg2;

        switch (gameState.result) {
            case WIN:
                int winnerId = gameState.winnerId;
                int loserId = gameState.getOpponentId(winnerId);

                TTTGamePlayerInfo winnerInfo = getGamePlayerInfo(winnerId, gamePlayersInfos);
                TTTGamePlayerInfo loserInfo = getGamePlayerInfo(loserId, gamePlayersInfos);

                TTTGame.Mark winningMark = gameState.getPlayerState(gameState.winnerId).mark;

                if (winningMark != null) {
                    msg = getString(R.string.game_result_win, winnerInfo.name, getString(winningMark.equals(TTTGame.Mark.CROSS) ? R.string.game_mark_cross : R.string.game_mark_nought));
                    msg2 = getString(R.string.game_result_quit, loserInfo.name, getString(winningMark.opposite().equals(TTTGame.Mark.CROSS) ? R.string.game_mark_cross : R.string.game_mark_nought));
                } else {
                    msg = getString(R.string.game_result_win2, winnerInfo.name);
                    msg2 = getString(R.string.game_result_quit2, loserInfo.name);
                }

                break;

            case TIE:
                msg = getString(R.string.game_result_tie);
                msg2 = "";
                break;

            default:
                logger.error("Game is not finished");
                msg = getString(R.string.game_error);
                msg2 = "";
                break;
        }

        String message = msg;

        if (gameState.quitterId != -1) {
            message = String.format(Locale.US, "%s\n%s", message, msg2);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.game_summary_title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void notifyMove(int x, int y) {
        if (eventListener != null) {
            eventListener.onMove(this, x, y);
        }
    }

    private void notifyQuit() {
        if (eventListener != null) {
            eventListener.onQuit(this);
        }
    }

    private void setBoardEnabled(boolean enabled) {
        for (int x = 0; x < TTTGame.BOARD_WIDTH; x++) {
            for (int y = 0; y < TTTGame.BOARD_HEIGHT; y++) {
                Button button = fieldsButtons.get(x).get(y);
                if (button.getText().length() == 0) {
                    button.setEnabled(enabled);
                } else {
                    button.setEnabled(false);
                }
            }
        }
    }

    @Override
    public void setEventListener(EventListener eventListener) {
        this.eventListener = eventListener;
    }

    private TTTGamePlayerInfo getGamePlayerInfo(int playerId, List<TTTGamePlayerInfo> gamePlayersInfos) {
        TTTGamePlayerInfo gamePlayerInfo = null;

        for (TTTGamePlayerInfo gpi : gamePlayersInfos) {
            if (gpi.id == playerId) {
                gamePlayerInfo = gpi;
                break;
            }
        }

        return gamePlayerInfo;
    }
}
