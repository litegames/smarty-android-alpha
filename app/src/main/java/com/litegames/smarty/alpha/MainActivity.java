package com.litegames.smarty.alpha;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.litegames.smarty.alpha.game.Version;
import com.litegames.smarty.alpha.game.tictactoe.TTTGame;
import com.litegames.smarty.sdk.GameRoom;
import com.litegames.smarty.sdk.GameSettings;
import com.litegames.smarty.sdk.LeaderboardActivity;
import com.litegames.smarty.sdk.MatchHistoryActivity;
import com.litegames.smarty.sdk.MatchMakingActivity;
import com.litegames.smarty.sdk.PlayersActivity;
import com.litegames.smarty.sdk.Smarty;
import com.litegames.smarty.sdk.SmartyProvider;
import com.litegames.smarty.sdk.UserProfileActivity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    public static final Logger logger = LoggerFactory.getLogger(MainActivity.class);

    public static final int REQUEST_CODE_CREATE_AND_JOIN_LOBBY = 100;
    public static final int REQUEST_CODE_JOIN_LOBBY = 101;

    private final ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(
                    new ActivityResultContracts.RequestPermission(),
                    isGranted -> {
                    }
            );

    private Smarty smarty;
    private GameSettings gameSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Request POST_NOTIFICATIONS permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            boolean postNotificationsPermissionGranted = ContextCompat.checkSelfPermission(
                    this, Manifest.permission.POST_NOTIFICATIONS
            ) == PackageManager.PERMISSION_GRANTED;

            if (!postNotificationsPermissionGranted) {
                requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS);
            }
        }

        smarty = SmartyProvider.getInstance().getSmarty();
        gameSettings = new GameSettings(2);

        String action = getIntent().getAction();
        if (action != null) {
            if (action.equals(MatchMakingActivity.ACTION_CREATE)) {
                Intent i = MatchMakingActivity.intentForCreateAndJoinLobbyAction(this,
                        gameSettings,
                        getIntent().getIntegerArrayListExtra(MatchMakingActivity.EXTRA_INVTEES_IDS));
                startActivityForResult(i, REQUEST_CODE_JOIN_LOBBY);
            } else if (action.equals(MatchMakingActivity.ACTION_JOIN)) {
                Intent i = MatchMakingActivity.intentForJoinLobbyAction(this,
                        getIntent().getIntExtra(MatchMakingActivity.EXTRA_LOBBY_ROOM_ID, -1),
                        getIntent().getStringExtra(MatchMakingActivity.EXTRA_LOBBY_ROOM_PASSWORD));
                startActivityForResult(i, REQUEST_CODE_JOIN_LOBBY);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void onSingleplayerClick(View view) {
        startActivity(GameActivity.intentForDefaultActionSinglePlayerMode(this));
    }

    public void onHotseatClick(View view) {
        startActivity(GameActivity.intentForDefaultActionHotSeatMode(this));
    }

    public void onMultiplayerClick(View view) {
        showMatchMaking(gameSettings);
    }

    public void onSettingsClick(View view) {
        startActivity(UserProfileActivity.intentForDefaultAction(this));
    }

    public void onMatchHistoryClick(View view) {
        startActivity(MatchHistoryActivity.intentForDefaultAction(this));
    }

    public void onBuddiesClick(View view) {
        startActivity(PlayersActivity.intentForDefaultAction(this));
    }

    public void onStatisticsClick(View view) {
        startActivity(StatisticsActivity.intentForDefaultAction(this));
    }

    public void onLeaderboardClick(View view) {
        startActivity(LeaderboardActivity.intentForDefaultAction(this, TTTGame.LEADERBOARD_IDENTIFIER));
    }

    private void startMultiplayer(GameRoom gameRoom) {
        startActivity(GameActivity.intentForDefaultActionMultiplayerMode(this, gameRoom));
    }

    public void onAboutClick(View view) {
        String msg = String.format(
                Locale.US,
                "SmartyAlpha version: %s\n" +
                        "SmartyAlpha gamelib version: %s\n" +
                        "SmartySDK version: %s\n",
                BuildConfig.VERSION_NAME,
                Version.NAME,
                Smarty.getVersion()
        );

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.main_about_button));
        builder.setMessage(msg);
        builder.setPositiveButton(getString(android.R.string.ok), null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showMatchMaking(GameSettings gameSettings) {
        showMatchMaking(gameSettings, null);
    }

    private void showMatchMaking(GameSettings gameSettings, List<Integer> inviteesIds) {
        Intent i = new Intent(this, MatchMakingActivity.class);
        i.putExtra(MatchMakingActivity.EXTRA_GAME_SETTINGS, gameSettings);
        if (inviteesIds != null) {
            i.putIntegerArrayListExtra(MatchMakingActivity.EXTRA_INVTEES_IDS, new ArrayList<Integer>(inviteesIds));
        }

        startActivityForResult(i, REQUEST_CODE_CREATE_AND_JOIN_LOBBY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CREATE_AND_JOIN_LOBBY || requestCode == REQUEST_CODE_JOIN_LOBBY) {
            if (resultCode == RESULT_OK) {
                int gameRoomId = data.getIntExtra(MatchMakingActivity.RESULT_OK_PARAM_GAME_ROOM_ID, -1);
                startMultiplayer(smarty.getGameManager().getGameRoom());
            }
        }
    }

}
