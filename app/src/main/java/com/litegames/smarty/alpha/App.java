package com.litegames.smarty.alpha;

import android.app.Application;
import android.content.Intent;

import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.litegames.smarty.sdk.AuthenticationListener;
import com.litegames.smarty.sdk.Error;
import com.litegames.smarty.sdk.GameCreateListener;
import com.litegames.smarty.sdk.GameJoinListener;
import com.litegames.smarty.sdk.MatchMakingActivity;
import com.litegames.smarty.sdk.Smarty;
import com.litegames.smarty.sdk.SmartyProvider;
import com.litegames.smarty.sdk.UserVariable;

import java.util.ArrayList;
import java.util.List;

public class App extends Application {

    private static final String SERVER_ADDRESS = "smarty02.lite-games.com";
    private static final int SERVER_PORT = 6666;
    private static final String ZONE_NAME = "SmartyAlphaZone";
    private static final int GAME_PROTOCOL_VERSION = 1;

    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
        crashlytics.setCustomKey("SmartySDKVersion", Smarty.getVersion());

        System.setProperty("java.net.preferIPv6Addresses", "false");



        SmartyProvider.initialize(this, SERVER_ADDRESS, SERVER_PORT, ZONE_NAME, GAME_PROTOCOL_VERSION, R.drawable.notify_icon, R.raw.notify);
        SmartyProvider smartyProvider = SmartyProvider.getInstance();

        // Example of handling new, backward-compatible, fictional 'tie proposals' feature.
        announceSupportedFeatures(smartyProvider.getSmarty());

        smartyProvider.addGameCreateListener(new GameCreateListener() {
            @Override
            public void onGameCreate(Object sender, List<Integer> inviteesIds) {
                Intent intent = new Intent(App.this, MainActivity.class);
                intent.setAction(MatchMakingActivity.ACTION_CREATE);
                if (inviteesIds != null) {
                    intent.putIntegerArrayListExtra(MatchMakingActivity.EXTRA_INVTEES_IDS, new ArrayList<Integer>(inviteesIds));
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                App.this.startActivity(intent);
            }
        });
        smartyProvider.addGameJoinListener(new GameJoinListener() {
            @Override
            public void onGameJoin(Object sender, int lobbyRoomId, String lobbyRoomPassword) {
                Intent intent = new Intent(App.this, MainActivity.class);
                intent.setAction(MatchMakingActivity.ACTION_JOIN);
                intent.putExtra(MatchMakingActivity.EXTRA_LOBBY_ROOM_ID, lobbyRoomId);
                intent.putExtra(MatchMakingActivity.EXTRA_LOBBY_ROOM_PASSWORD, lobbyRoomPassword);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                App.this.startActivity(intent);
            }
        });

        FacebookSdk.setIsDebugEnabled(true);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.CACHE);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.DEVELOPER_ERRORS);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.GRAPH_API_DEBUG_WARNING);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.GRAPH_API_DEBUG_INFO);
    }

    private void announceSupportedFeatures(Smarty smarty) {
        smarty.addAuthenticationListener(new AuthenticationListener() {
            @Override
            public void onLogin(Smarty smarty) {
                List<UserVariable> userVariables = new ArrayList<>();
                userVariables.add(UserVariable.CreateBoolVariable("supportsTieProposals", true));
                smarty.getMySelf().setUserVariables(userVariables);
            }

            @Override
            public void onLoginError(Smarty smarty, Error error) {

            }

            @Override
            public void onLogout(Smarty smarty) {

            }
        });
    }

}
