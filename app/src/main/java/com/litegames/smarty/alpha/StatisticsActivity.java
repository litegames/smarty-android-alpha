package com.litegames.smarty.alpha;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import com.litegames.smarty.sdk.RequestResultFragment;
import com.litegames.smarty.sdk.Smarty;
import com.litegames.smarty.sdk.UserStatistics;
import com.litegames.smarty.sdk.internal.prefs.PreferencesArrayAdapter;
import com.litegames.smarty.sdk.internal.prefs.PreferencesItem;
import com.litegames.smarty.sdk.internal.prefs.items.ButtonItemViewProvider;
import com.litegames.smarty.sdk.pendingresult.PendingResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class StatisticsActivity extends AppCompatActivity {

    private static final String FRAGMENT_TAG_REQUEST_RESULT = "RequestResultFragmentTag";

    private static final Logger logger = LoggerFactory.getLogger(StatisticsActivity.class);

    public static Intent intentForDefaultAction(Context context) {
        return new Intent(context, StatisticsActivity.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Setup title bar
        setTitle(R.string.statistics_title);

        // Setup view
        setContentView(R.layout.smarty_activity_statistics);

        if (getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_REQUEST_RESULT) == null) {
            Bundle params = new Bundle();
            RequestResultFragment fragment = RequestResultFragment.newInstance(params, com.litegames.smarty.sdk.R.string.smarty__common__text_no_results, new StatisticsImplementation());

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.smarty__activity_statistics_summary__content, fragment, FRAGMENT_TAG_REQUEST_RESULT);
            ft.commit();
        }
    }

    private PendingResult<UserStatistics> performRequest(Smarty smarty) {
        return smarty.getStatisticsManager().getMyStatistics();
    }


    private static class StatisticsImplementation implements RequestResultFragment.Implementation<UserStatistics> {

        public StatisticsImplementation() {
        }

        @Override
        public PendingResult<UserStatistics> performRequest(Smarty smarty, int page, int pageSize, Bundle params) {
            return smarty.getStatisticsManager().getMyStatistics();
        }

        @Override
        public void presentResult(Smarty smarty, UserStatistics stats, PreferencesArrayAdapter listAdapter, RequestResultFragment.Listener listener) {
            Context context = listAdapter.getContext();

            listAdapter.add(createStatisticsItem(context.getString(R.string.statistics_wins, stats.getWins())));
            listAdapter.add(createStatisticsItem(context.getString(R.string.statistics_losses, stats.getLosses())));
            listAdapter.add(createStatisticsItem(context.getString(R.string.statistics_ties, stats.getTies())));
        }

        private PreferencesItem createStatisticsItem(final String text) {
            final PreferencesItem item = new PreferencesItem(true, new ButtonItemViewProvider(
                    new ButtonItemViewProvider.DataProvider() {
                        @Override
                        public String getText() {
                            return text;
                        }
                    }), null);
            return item;
        }
    }

}
